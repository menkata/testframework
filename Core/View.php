<?php
namespace Core;

class View implements ViewInterface {
	public $controller_name;
	public $action_name;
	
	public function __construct($controller_name,$action_name)
	{
		$this->controller_name = $controller_name;
		$this->action_name = $action_name;
	}
	
	public function render()
	{
		include ('view/' 
		.$this->controller_name 
		.'/' 
		.$this->action_name 
		.'.php');
	}
}
?>